const String kHomeRoute = "HomeScreen";
const String kLoginRoute = 'LoginScreen';
const String kRegisterOptionRoute = 'RegisterOptionScreen';
const String kForgotPasswordRoute = 'ForgotPasswordScreen';
const String kSignupRoute = 'SignupScreen';
