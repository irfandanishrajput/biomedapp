const String kRegisterText = 'REGISTER';
const String kRegisterDescriptionText =
    'Register yourself & be a part of citizen scientist world.';
const String kRegisterAsCitizenScientist = 'As a Citizen Scientist';
const String kRegisterAsManager = 'As a Biosphere Reserve Manager';
const String kRegisterAsResearcher = 'As a Researcher';
const String kContinueAsUser = 'Continue as a user';
const String kAlreadyHaveAccount = 'Already have an account?';
const String kDontHaveAccount = 'Don\'t have an account?';
const String kLoginHere = 'LOGIN HERE';
const String kRegisterHere = 'REGISTER HERE';

const String kLoginText = 'Login';
const String kLoginDescriptionText =
    'Let\'s jump into the citizen scientist\nworld.';

const String kForgotPassword = 'Forgot Password?';
const String kKeepMeLogin = 'Keep me logged in';
const String kUsername = 'Username';
const String kPassword = 'Password';
const String kSendText = 'Send';
const String kEmailText = 'Email Address';
const String kForgotYourPasswordText = 'Forgot Your Password';
const String kForgotYourPasswordDescriptionText =
    'Lost your password? No worries, we are here to help you. Enter your email address & we will send you password recovery link.';
const String kSignupText = 'Sign Up';
const String kSignupDescriptionText = 'Sign up using social networks';
const String kNameText = 'Name';
const String kFamilyNameText = 'Family Name';
const String kPhoneNumberText = 'Phone Number';
const String kLocationText = 'Location';
const String kEnterSkillsText = 'Enter Skills';
const String kEnterInterestsText = 'Enter Interests';
const String kOrFillTheFormText = 'Or Fill The Form';
const String kVolunteerOpportunitiesText = 'Volunteer Opportunities';
const String kNewProjectsText = 'New Projects';
const String kOngingProjectsText = 'Ongoing Projects';
const String kCompletedProjectsText = 'Completed Projects';
const String kMakeObservationsText = 'Make Observations';
