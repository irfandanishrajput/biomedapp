import 'package:flutter/cupertino.dart';

const Color kPrimaryColor = Color(0xFF42A462);
const Color kSecondaryColor = Color(0xEE984F23);
const Color TextFormFieldFillColor = Color(0xFFE9E9E9);
