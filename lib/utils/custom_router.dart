import 'package:edu_bio_med/screens/forgot_password_screen.dart';
import 'package:edu_bio_med/screens/home_screen.dart';
import 'package:edu_bio_med/screens/register_option_screen.dart';
import 'package:edu_bio_med/screens/login_screen.dart';
import 'package:edu_bio_med/screens/signup_screen.dart';
import 'package:edu_bio_med/utils/constants/route_constants.dart';
import 'package:flutter/material.dart';

class CustomRouter {
  static Route<dynamic> allRoutes(RouteSettings setting) {
    switch (setting.name) {
      case kRegisterOptionRoute:
        return MaterialPageRoute(builder: (_) => RegisterOptionScreen());
      case kLoginRoute:
        return MaterialPageRoute(builder: (_) => LoginScreen());
      case kForgotPasswordRoute:
        return MaterialPageRoute(builder: (_) => ForgotPasswordScreen());
      case kSignupRoute:
        return MaterialPageRoute(builder: (_) => SignupScreen());
      case kHomeRoute:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      default:
        return MaterialPageRoute(builder: (_) => RegisterOptionScreen());
    }
  }
}
