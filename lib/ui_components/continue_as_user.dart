import 'package:edu_bio_med/utils/constants/text_constants.dart';
import 'package:flutter/material.dart';

class ContinueAsUser extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {},
      child: Text(
        kContinueAsUser,
        style: TextStyle(fontSize: 15),
      ),
    );
  }
}
