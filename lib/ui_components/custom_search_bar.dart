import 'package:edu_bio_med/utils/constants/color_constants.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class CustomSearchBar extends StatefulWidget {
  final List<String> items;

  const CustomSearchBar({
    @required this.items,
  });
  @override
  _CustomSearchBarState createState() => _CustomSearchBarState();
}

class _CustomSearchBarState extends State<CustomSearchBar> {
  String _dropDownValue;

  @override
  void initState() {
    _dropDownValue = widget.items[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(flex: 3, child: buildDropdown()),
        Expanded(flex: 4, child: buildSearchTextField())
      ],
    );
  }

  Widget buildSearchTextField() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 1.0),
        decoration: BoxDecoration(
          color: TextFormFieldFillColor,
          borderRadius: BorderRadius.only(
              topRight: Radius.circular(6.0),
              bottomRight: Radius.circular(6.0)),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 5.0),
          child: TextField(
            keyboardType: TextInputType.text,
            cursorColor: Colors.blue[900],
            decoration: InputDecoration(
              border: InputBorder.none,
              labelText: 'Search',
              labelStyle: TextStyle(fontSize: 16.0, color: Colors.blue[900]),
            ),
          ),
        ));
  }

  Widget buildDropdown() {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 6.0),
        decoration: BoxDecoration(
          color: Colors.grey[300],
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(6.0), bottomLeft: Radius.circular(6.0)),
        ),
        child: Padding(
            padding: const EdgeInsets.only(
              left: 8.0,
            ),
            child: Theme(
              data: Theme.of(context).copyWith(
                canvasColor: Colors.grey[300],
              ),
              child: DropdownButton<String>(
                isExpanded: true,
                value: _dropDownValue,
                icon: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0, right: 8),
                  child: Transform.rotate(
                    angle: -math.pi / 2,
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 13.0,
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                ),
                style: TextStyle(color: Colors.black87, fontSize: 16.0),
                underline: Container(),
                onChanged: (String val) {
                  setState(() {
                    _dropDownValue = val;
                  });
                },
                selectedItemBuilder: (BuildContext context) {
                  return widget.items.map((String value) {
                    return Padding(
                      padding: const EdgeInsets.only(top: 14.0, left: 4.0),
                      child: Text(
                        _dropDownValue,
                        style: TextStyle(
                            fontSize: 16.0,
                            color: Theme.of(context).accentColor),
                      ),
                    );
                  }).toList();
                },
                items:
                    widget.items.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        value,
                      ),
                    ),
                  );
                }).toList(),
              ),
            )));
  }
}
