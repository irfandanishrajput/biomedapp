import 'package:edu_bio_med/utils/constants/color_constants.dart';
import 'package:flutter/material.dart';
import 'dart:math' as math;

class CustomDropDown extends StatefulWidget {
  final List<String> items;

  CustomDropDown({@required this.items});

  @override
  _CustomDropDownState createState() => _CustomDropDownState();
}

class _CustomDropDownState extends State<CustomDropDown> {
  String _dropDownValue;

  @override
  void initState() {
    _dropDownValue = widget.items[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(vertical: 4.0),
        decoration: BoxDecoration(
          color: TextFormFieldFillColor,
          borderRadius: BorderRadius.circular(6.0),
        ),
        child: Padding(
            padding: const EdgeInsets.only(left: 10.0, right: 10.0),
            child: Theme(
              data: Theme.of(context).copyWith(
                canvasColor: Colors.grey[300],
              ),
              child: DropdownButton<String>(
                isExpanded: true,
                value: _dropDownValue,
                icon: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0, right: 18),
                  child: Transform.rotate(
                    angle: -math.pi / 2,
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 16.0,
                      color: Colors.black87,
                    ),
                  ),
                ),
                iconSize: 24,
                style: TextStyle(color: Colors.black87, fontSize: 15.0),
                underline: Container(),
                onChanged: (String val) {
                  setState(() {
                    _dropDownValue = val;
                  });
                },
                selectedItemBuilder: (BuildContext context) {
                  return widget.items.map((String value) {
                    return Padding(
                      padding: const EdgeInsets.only(top: 14.0, left: 4.0),
                      child: Text(
                        _dropDownValue,
                        style: TextStyle(
                            fontSize: 15.0,
                            color: Theme.of(context).accentColor),
                      ),
                    );
                  }).toList();
                },
                items:
                    widget.items.map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        value,
                      ),
                    ),
                  );
                }).toList(),
              ),
            )));
  }
}
