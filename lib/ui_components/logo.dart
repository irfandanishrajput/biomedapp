import 'package:edu_bio_med/utils/constants/image_constants.dart';
import 'package:flutter/material.dart';

class Logo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image.asset(kLogoImage);
  }
}
