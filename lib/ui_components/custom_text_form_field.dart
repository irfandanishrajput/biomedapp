import 'package:edu_bio_med/utils/constants/color_constants.dart';
import 'package:flutter/material.dart';

class CustomTextFormField extends StatelessWidget {
  final String labelText;
  final bool isPasswordField;
  final TextInputType inputType;

  const CustomTextFormField(
      {@required this.labelText,
      @required this.isPasswordField,
      @required this.inputType});
  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: TextFormFieldFillColor,
          borderRadius: new BorderRadius.circular(6.0),
        ),
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 5.0),
          child: TextFormField(
            obscureText: isPasswordField,
            keyboardType: inputType,
            cursorColor: Colors.blue[900],
            decoration: InputDecoration(
              border: InputBorder.none,
              labelText: labelText,
              labelStyle: TextStyle(fontSize: 14.0, color: Colors.blue[900]),
            ),
          ),
        ));
  }
}
