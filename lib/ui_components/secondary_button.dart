import 'package:flutter/material.dart';

class SecondaryButton extends StatelessWidget {
  final String buttonLabel;
  final Function onPress;
  final EdgeInsets padding;

  const SecondaryButton(
      {@required this.buttonLabel,
      @required this.onPress,
      @required this.padding});
  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
          padding: padding, backgroundColor: Theme.of(context).accentColor),
      onPressed: onPress,
      child: Text(
        buttonLabel,
        style: TextStyle(color: Colors.white, fontSize: 15.0),
      ),
    );
  }
}
