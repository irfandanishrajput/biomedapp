import 'package:edu_bio_med/utils/constants/text_constants.dart';
import 'package:flutter/material.dart';

class AuthFooter extends StatelessWidget {
  final String accountText;
  final String accountActionText;
  final Function onTap;

  const AuthFooter(
      {@required this.accountText,
      @required this.accountActionText,
      @required this.onTap});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildContinueAsUser(),
        SizedBox(
          height: 30.0,
        ),
        AuthAccountFooter(
          accountText: accountText,
          accountActionText: accountActionText,
          onTap: onTap,
        ),
      ],
    );
  }

  Widget buildContinueAsUser() {
    return GestureDetector(
      onTap: () {},
      child: Text(
        kContinueAsUser,
        style: TextStyle(fontSize: 15),
      ),
    );
  }
}

class AuthAccountFooter extends StatelessWidget {
  final String accountText;
  final String accountActionText;
  final Function onTap;

  AuthAccountFooter(
      {@required this.accountText,
      @required this.accountActionText,
      @required this.onTap});
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(accountText),
        SizedBox(
          width: 5.0,
        ),
        GestureDetector(
          onTap: onTap,
          child: Text(
            accountActionText.toUpperCase(),
            style: TextStyle(
                color: Theme.of(context).accentColor,
                fontWeight: FontWeight.bold),
          ),
        )
      ],
    );
  }
}
