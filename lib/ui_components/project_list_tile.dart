import 'package:edu_bio_med/ui_components/secondary_button.dart';
import 'package:flutter/material.dart';

class ProjectListTile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Stack(
            children: [
              Container(
                height: 170,
                child: Image.asset(
                  'assets/images/biosphereIndia.jpg',
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                height: 35,
                width: 95,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8.0),
                decoration:
                    BoxDecoration(color: Colors.white.withOpacity(0.65)),
                child: Text(
                  'New',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Positioned(
                right: 0,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.green.withOpacity(0.60),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 15,
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.favorite,
                          color: Colors.white,
                          size: 28,
                        ),
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.share,
                          color: Colors.white,
                          size: 28,
                        ),
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.person_add_alt_1,
                          color: Colors.white,
                          size: 30,
                        ),
                        onPressed: () {},
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color: Colors.grey.shade200,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 15.0,
                ),
                Text(
                  'St kilda Mangrove & Saltmarsh monitoring',
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.black87),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      'Atlas of Living Australia',
                      style: TextStyle(fontSize: 16),
                    ),
                    SizedBox(
                      width: 12.0,
                    ),
                    Icon(
                      Icons.location_on,
                      size: 18.0,
                      color: Theme.of(context).accentColor,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      'Australia',
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                Row(
                  children: [
                    Text(
                      'Project Owner:',
                      style: TextStyle(fontWeight: FontWeight.w500),
                    ),
                    SizedBox(
                      width: 6,
                    ),
                    Text(
                      'John Duo',
                      style: TextStyle(color: Colors.black54),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                RichText(
                    text: TextSpan(
                        text:
                            'Start with the \"Beginner\'s Practice\" to learn how to identify white blood cells. Once ready ',
                        style: TextStyle(color: Colors.black54),
                        children: [
                      TextSpan(
                        text: 'Learn more.',
                        style: TextStyle(
                          color: Theme.of(context).accentColor,
                          decoration: TextDecoration.underline,
                        ),
                      )
                    ])),
                SizedBox(
                  height: 12,
                ),
                Container(
                  child: Text(
                    '#Environmental #Health #Atmosphere',
                  ),
                ),
                SizedBox(
                  height: 15,
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 10.0),
            decoration: BoxDecoration(color: Colors.grey.shade300),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: 200,
                      child: Text(
                        'Ferox Australis Biosphere Reserve',
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Colors.black87,
                            fontSize: 16),
                      ),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      children: [
                        Text(
                          'Started',
                          style: TextStyle(color: Colors.black87),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          '4.9 months ago',
                          style: TextStyle(color: Colors.black54),
                        )
                      ],
                    ),
                  ],
                ),
                SecondaryButton(
                  buttonLabel: 'Follow Project',
                  onPress: () {},
                  padding:
                      EdgeInsets.symmetric(horizontal: 35.0, vertical: 10.0),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
