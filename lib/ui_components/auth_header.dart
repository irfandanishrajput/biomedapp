import 'package:flutter/material.dart';

class AuthHeader extends StatelessWidget {
  final String heading;
  final String description;
  final double descriptionPadding;

  const AuthHeader(
      {@required this.heading,
      @required this.description,
      @required this.descriptionPadding});
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(
          heading.toUpperCase(),
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 15.0,
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: descriptionPadding),
          child: Text(
            description,
            textAlign: TextAlign.center,
            style: TextStyle(height: 1.5),
          ),
        ),
      ],
    );
  }
}
