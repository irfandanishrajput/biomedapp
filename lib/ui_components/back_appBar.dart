import 'package:flutter/material.dart';

Widget customBackAppBar({
  String title = ' ',
}) {
  return AppBar(
    backgroundColor: Colors.white,
    elevation: 0.0,
    iconTheme: IconThemeData(
      color: Colors.black,
    ),
    title: Text(
      title,
      style: TextStyle(color: Colors.black),
    ),
  );
}
