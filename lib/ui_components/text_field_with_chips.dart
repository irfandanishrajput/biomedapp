import 'package:edu_bio_med/utils/constants/color_constants.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class TextFieldWithChips extends StatefulWidget {
  List chipList;
  final String labelText;
  final TextEditingController controller = TextEditingController();
  final FocusNode focusNode = FocusNode();
  TextFieldWithChips({@required this.chipList, @required this.labelText});
  @override
  _TextFieldWithChipsState createState() => _TextFieldWithChipsState();
}

class _TextFieldWithChipsState extends State<TextFieldWithChips> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
            decoration: BoxDecoration(
              color: TextFormFieldFillColor,
              borderRadius: BorderRadius.circular(6.0),
            ),
            child: Row(
              children: [
                Expanded(
                  flex: 5,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20.0, right: 5.0),
                    child: TextFormField(
                      focusNode: widget.focusNode,
                      controller: widget.controller,
                      keyboardType: TextInputType.text,
                      cursorColor: Colors.blue[900],
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        labelText: widget.labelText,
                        labelStyle:
                            TextStyle(fontSize: 14.0, color: Colors.blue[900]),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: GestureDetector(
                    onTap: () {
                      setState(() {
                        if (widget.controller.value.text.isNotEmpty) {
                          widget.chipList.add(widget.controller.value.text);
                          widget.controller.clear();
                          widget.focusNode.unfocus();
                        }
                      });
                    },
                    child: Container(
                      height: 55,
                      decoration: BoxDecoration(
                        color: Colors.blue[900],
                        borderRadius: new BorderRadius.only(
                            topRight: Radius.circular(5),
                            bottomRight: Radius.circular(5)),
                      ),
                      child: Icon(
                        Icons.add,
                        color: Colors.white,
                        size: 35,
                      ),
                    ),
                  ),
                )
              ],
            )),
        SizedBox(
          height: 8.0,
        ),
        buildUsersInterestRow()
      ],
    );
  }

  Widget buildUsersInterestRow() {
    return Wrap(
      children: widget.chipList
          .map((e) => Container(
                padding: EdgeInsets.symmetric(
                  horizontal: 5,
                ),
                child: Chip(
                  padding:
                      EdgeInsets.symmetric(vertical: 12.0, horizontal: 8.0),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4))),
                  label: Text(
                    e,
                    style: TextStyle(fontSize: 17.0),
                  ),
                  deleteIcon: Icon(
                    Icons.close,
                    size: 18.0,
                  ),
                  onDeleted: () {
                    setState(() {
                      widget.chipList.remove(e);
                    });
                  },
                ),
              ))
          .toList()
          .cast<Widget>(),
    );
  }
}
