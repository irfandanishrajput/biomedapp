import 'package:edu_bio_med/utils/constants/color_constants.dart';
import 'package:edu_bio_med/utils/custom_router.dart';
import 'package:edu_bio_med/utils/constants/route_constants.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'EduBioMed',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: kPrimaryColor,
          accentColor: Colors.blue[900],
          scaffoldBackgroundColor: Colors.white,
          dividerTheme: DividerThemeData(
              thickness: 0.8, color: Colors.black54, space: 30.0)),
      onGenerateRoute: CustomRouter.allRoutes,
      initialRoute: kHomeRoute,
    );
  }
}
