import 'package:edu_bio_med/ui_components/auth_footer.dart';
import 'package:edu_bio_med/ui_components/auth_header.dart';
import 'package:edu_bio_med/ui_components/logo.dart';
import 'package:edu_bio_med/ui_components/primary_button.dart';
import 'package:edu_bio_med/utils/constants/route_constants.dart';
import 'package:edu_bio_med/utils/constants/text_constants.dart';
import 'package:flutter/material.dart';

class RegisterOptionScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.only(
            left: 50.0, right: 50.0, bottom: 30.0, top: 150.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Logo(),
                SizedBox(
                  height: 60,
                ),
                AuthHeader(
                  heading: kRegisterText,
                  description: kRegisterDescriptionText,
                  descriptionPadding: 10.0,
                ),
                SizedBox(
                  height: 80.0,
                ),
                buildRegisterOptions(),
              ],
            ),
            AuthFooter(
                accountText: kAlreadyHaveAccount,
                accountActionText: kLoginHere,
                onTap: () {
                  Navigator.pushNamed(context, kLoginRoute);
                })
          ],
        ),
      ),
    );
  }

  Widget buildRegisterOptions() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        PrimaryButton(
          buttonLabel: kRegisterAsCitizenScientist,
          onPress: () {},
          padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 18.0),
        ),
        SizedBox(
          height: 20.0,
        ),
        PrimaryButton(
          buttonLabel: kRegisterAsManager,
          onPress: () {},
          padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 18.0),
        ),
        SizedBox(
          height: 20.0,
        ),
        PrimaryButton(
          buttonLabel: kRegisterAsResearcher,
          onPress: () {},
          padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 18.0),
        ),
      ],
    );
  }
}
