import 'package:edu_bio_med/ui_components/primary_button.dart';
import 'package:flutter/material.dart';

class VolunteerOpportunitiesScreen extends StatelessWidget {
  //Pass the opportunities list from here to the listview builder
  @override
  Widget build(BuildContext context) {
    return VolunteerOpportunities();
  }
}

class VolunteerOpportunities extends StatefulWidget {
  @override
  _VolunteerOpportunitiesState createState() => _VolunteerOpportunitiesState();
}

class _VolunteerOpportunitiesState extends State<VolunteerOpportunities> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        itemCount: 10,
        itemBuilder: (context, index) {
          return buildVolunteerOpportunitiesListTile();
        });
  }

  Widget buildVolunteerOpportunitiesListTile() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Stack(
            children: [
              Container(
                height: 170,
                child: Image.asset(
                  'assets/images/biosphereIndia.jpg',
                  fit: BoxFit.fill,
                ),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 8.0),
                decoration:
                    BoxDecoration(color: Colors.white.withOpacity(0.65)),
                child: Text(
                  'Ongoing',
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 16.0,
                      fontWeight: FontWeight.w600),
                ),
              ),
              Positioned(
                right: 0,
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.green.withOpacity(0.60),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 15,
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.favorite,
                          color: Colors.white,
                          size: 28,
                        ),
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.share,
                          color: Colors.white,
                          size: 28,
                        ),
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.person_add_alt_1,
                          color: Colors.white,
                          size: 30,
                        ),
                        onPressed: () {},
                      ),
                      SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 10),
            color: Colors.grey.shade200,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 15.0,
                ),
                Text(
                  'Monkey Health Explorer',
                  style: TextStyle(
                      fontSize: 18.0,
                      fontWeight: FontWeight.w500,
                      color: Colors.black87),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      'Organization Name',
                      style: TextStyle(fontSize: 16),
                    ),
                    SizedBox(
                      width: 12.0,
                    ),
                    Icon(
                      Icons.location_on,
                      size: 18.0,
                      color: Theme.of(context).accentColor,
                    ),
                    SizedBox(
                      width: 5.0,
                    ),
                    Text(
                      'Sydney, Australia',
                      style: TextStyle(fontSize: 16),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10.0,
                ),
                Container(
                  child: Text(
                    'We have volunteers of all ages and back grounds - from school leavers to retired professionals and for periods of a week to 3 years.',
                  ),
                ),
                SizedBox(
                  height: 18,
                ),
                Text(
                  'Skills Needed:',
                  style: TextStyle(fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Text(
                    'Environmental Research, Good Communication',
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        Text(
                          'Project Owner',
                          style: TextStyle(fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          width: 6,
                        ),
                        Text(
                          'John Duo',
                          style: TextStyle(color: Colors.black54),
                        ),
                      ],
                    ),
                    Text(
                      '3 Training Sessions',
                      style: TextStyle(
                        color: Colors.green.shade500,
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 15,
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 15.0),
            decoration: BoxDecoration(color: Colors.grey.shade300),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Nilgitl Biosphere Reserve',
                      style: TextStyle(
                          fontWeight: FontWeight.w500, color: Colors.black87),
                    ),
                    SizedBox(
                      height: 5.0,
                    ),
                    Row(
                      children: [
                        Text(
                          'Start Date:',
                          style: TextStyle(color: Colors.black87),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'Jan 16, 2021',
                          style: TextStyle(color: Colors.black54),
                        )
                      ],
                    ),
                  ],
                ),
                PrimaryButton(
                  buttonLabel: 'Join Training',
                  onPress: () {},
                  padding:
                      EdgeInsets.symmetric(horizontal: 35.0, vertical: 10.0),
                )
              ],
            ),
          ),
          SizedBox(
            height: 20.0,
          )
        ],
      ),
    );
  }
}
