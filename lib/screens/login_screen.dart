import 'package:edu_bio_med/ui_components/auth_footer.dart';
import 'package:edu_bio_med/ui_components/auth_header.dart';
import 'package:edu_bio_med/ui_components/custom_text_form_field.dart';
import 'package:edu_bio_med/ui_components/logo.dart';
import 'package:edu_bio_med/ui_components/secondary_button.dart';
import 'package:edu_bio_med/utils/constants/route_constants.dart';
import 'package:edu_bio_med/utils/constants/text_constants.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Login();
  }
}

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  bool checkboxValue = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Padding(
        padding:
            EdgeInsets.only(left: 20.0, right: 20.0, top: 80.0, bottom: 30.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Logo(),
                SizedBox(
                  height: 50,
                ),
                AuthHeader(
                  heading: kLoginText,
                  description: kLoginDescriptionText,
                  descriptionPadding: 10.0,
                ),
                SizedBox(
                  height: 50.0,
                ),
                buildLoginForm(),
                SizedBox(
                  height: 10.0,
                ),
                buildCheckbox(),
                SizedBox(
                  height: 20.0,
                ),
                SecondaryButton(
                  buttonLabel: kLoginText,
                  onPress: () {},
                  padding:
                      EdgeInsets.symmetric(horizontal: 40.0, vertical: 18.0),
                ),
                SizedBox(
                  height: 20.0,
                ),
                buildForgotPassword()
              ],
            ),
            AuthFooter(
                accountText: kDontHaveAccount,
                accountActionText: kRegisterHere,
                onTap: () {
                  Navigator.pushNamed(context, kSignupRoute);
                })
          ],
        ),
      ),
    );
  }

  Widget buildForgotPassword() {
    return GestureDetector(
      onTap: () => Navigator.pushNamed(context, kForgotPasswordRoute),
      child: Text(
        kForgotPassword,
        style: TextStyle(
            color: Theme.of(context).accentColor, fontWeight: FontWeight.bold),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget buildCheckbox() {
    return CheckboxListTile(
      contentPadding: EdgeInsets.all(0),
      controlAffinity: ListTileControlAffinity.leading,
      title: Text(
        kKeepMeLogin,
        style: TextStyle(color: Colors.blue[900]),
      ),
      value: checkboxValue,
      activeColor: Theme.of(context).primaryColor,
      onChanged: (value) {
        setState(() {
          checkboxValue = value;
        });
      },
    );
  }

  Widget buildLoginForm() {
    return Form(
        child: Column(
      children: [
        CustomTextFormField(
          labelText: kUsername,
          isPasswordField: false,
          inputType: TextInputType.name,
        ),
        SizedBox(
          height: 20,
        ),
        CustomTextFormField(
          labelText: kPassword,
          isPasswordField: true,
          inputType: TextInputType.text,
        ),
      ],
    ));
  }
}
