import 'package:edu_bio_med/ui_components/auth_footer.dart';
import 'package:edu_bio_med/ui_components/auth_header.dart';
import 'package:edu_bio_med/ui_components/custom_dropdown.dart';
import 'package:edu_bio_med/ui_components/custom_text_form_field.dart';
import 'package:edu_bio_med/ui_components/secondary_button.dart';
import 'package:edu_bio_med/ui_components/text_field_with_chips.dart';
import 'package:edu_bio_med/utils/constants/image_constants.dart';
import 'package:edu_bio_med/utils/constants/route_constants.dart';
import 'package:flutter/material.dart';
import 'package:edu_bio_med/utils/constants/text_constants.dart';

class SignupScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Signup();
  }
}

class Signup extends StatefulWidget {
  @override
  _SignupState createState() => _SignupState();
}

class _SignupState extends State<Signup> {
  List<String> interests = [];
  List<String> skills = [];
  List<String> dropDownItems = [
    'Biosphere Reserve of Interes',
    'Citizen Scientist',
    'Manager'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20.0),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                AuthHeader(
                    heading: kSignupText,
                    description: kSignupDescriptionText,
                    descriptionPadding: 10),
                SizedBox(
                  height: 30.0,
                ),
                buildSocialSignupOptionRow(),
                SizedBox(
                  height: 20.0,
                ),
                buildDivider(),
                SizedBox(height: 35.0),
                buildSignupForm(),
                SizedBox(
                  height: 20.0,
                ),
                SecondaryButton(
                  buttonLabel: kSignupText,
                  onPress: () {},
                  padding:
                      EdgeInsets.symmetric(horizontal: 40.0, vertical: 18.0),
                ),
                SizedBox(
                  height: 50.0,
                ),
                AuthAccountFooter(
                  accountText: kAlreadyHaveAccount,
                  accountActionText: kLoginHere,
                  onTap: () => Navigator.pushNamed(context, kLoginRoute),
                ),
                SizedBox(
                  height: 20.0,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildSocialSignupOptionRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        buildSocialSignupAvatar(image: kFacebookLogo),
        SizedBox(
          width: 20.0,
        ),
        buildSocialSignupAvatar(image: kFacebookLogo),
        SizedBox(
          width: 20.0,
        ),
        buildSocialSignupAvatar(image: kFacebookLogo),
        SizedBox(
          width: 20.0,
        ),
        buildSocialSignupAvatar(image: kFacebookLogo),
      ],
    );
  }

  Widget buildSocialSignupAvatar({@required String image}) {
    return CircleAvatar(
      maxRadius: 25.0,
      child: Image.asset(
        image,
      ),
    );
  }

  Widget buildDivider() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(child: Divider()),
        SizedBox(
          width: 10.0,
        ),
        Text(kOrFillTheFormText),
        SizedBox(
          width: 10.0,
        ),
        Expanded(child: Divider()),
      ],
    );
  }

  Widget buildSignupForm() {
    return Form(
        child: Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        CustomTextFormField(
            labelText: kNameText,
            isPasswordField: false,
            inputType: TextInputType.name),
        SizedBox(
          height: 20.0,
        ),
        CustomTextFormField(
            labelText: kFamilyNameText,
            isPasswordField: false,
            inputType: TextInputType.name),
        SizedBox(
          height: 20.0,
        ),
        CustomTextFormField(
            labelText: kPhoneNumberText,
            isPasswordField: false,
            inputType: TextInputType.number),
        SizedBox(
          height: 20.0,
        ),
        CustomTextFormField(
            labelText: kLocationText,
            isPasswordField: false,
            inputType: TextInputType.text),
        SizedBox(
          height: 20.0,
        ),
        CustomDropDown(
          items: ['Biosphere Reserve of Interest', 'Citizen Scientist'],
        ),
        SizedBox(
          height: 20.0,
        ),
        TextFieldWithChips(
          labelText: kEnterInterestsText,
          chipList: interests,
        ),
        SizedBox(
          height: 20.0,
        ),
        TextFieldWithChips(
          labelText: kEnterSkillsText,
          chipList: skills,
        ),
      ],
    ));
  }
}
