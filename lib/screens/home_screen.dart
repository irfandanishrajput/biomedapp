import 'package:edu_bio_med/screens/volunteer_opportunities_screen.dart';
import 'package:edu_bio_med/ui_components/custom_search_bar.dart';
import 'package:edu_bio_med/ui_components/project_list_tile.dart';
import 'package:edu_bio_med/utils/constants/image_constants.dart';
import 'package:edu_bio_med/utils/constants/text_constants.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Home();
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: DefaultTabController(
        initialIndex: 0,
        length: 4,
        child: Scaffold(
            drawer: Drawer(),
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              elevation: 0.0,
              backgroundColor: Colors.white,
              leading: Padding(
                padding: const EdgeInsets.only(top: 5.0, left: 0.0),
                child: Builder(
                  builder: (context) => IconButton(
                    icon: Icon(
                      Icons.menu,
                      color: Colors.black87,
                      size: 35.0,
                    ),
                    onPressed: () => Scaffold.of(context).openDrawer(),
                  ),
                ),
              ),
              leadingWidth: 30.0,
              actions: [
                Padding(
                  padding:
                      const EdgeInsets.only(top: 5.0, left: 8.0, right: 5.0),
                  child: IconButton(
                    icon: Icon(
                      Icons.filter_list_outlined,
                      color: Colors.black87,
                      size: 35.0,
                    ),
                    onPressed: () {},
                  ),
                )
              ],
              titleSpacing: 0.0,
              title: Transform.scale(
                scale: 0.88,
                child: Padding(
                  padding: const EdgeInsets.only(top: 5.0, left: 8.0),
                  child: Image.asset(kLogoImage),
                ),
              ),
            ),
            body: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 15.0,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: CustomSearchBar(
                    items: [
                      'Research Project',
                      'Bioshpere Reserve',
                      'Citizen Scientist',
                      'Biosphere Reserve Manager',
                    ],
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                buildMakeObservationHeader(),
                SizedBox(
                  height: 25.0,
                ),
                buildTabbar(),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 0.60,
                  child: TabBarView(children: [
                    VolunteerOpportunities(),
                    ProjectListTile(),
                    VolunteerOpportunities(),
                    VolunteerOpportunities(),
                  ]),
                ),
                // Expanded(child: buildListView(context))
              ],
            )),
      ),
    );
  }

  Widget buildTabbar() {
    return Material(
      elevation: 1.5,
      child: Container(
        height: 80.0,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: TabBar(
              labelColor: Theme.of(context).accentColor,
              unselectedLabelColor: Colors.black87,
              isScrollable: true,
              labelStyle: TextStyle(fontSize: 19.0),
              tabs: [
                Tab(
                  text: kVolunteerOpportunitiesText,
                ),
                Tab(
                  text: kNewProjectsText,
                ),
                Tab(
                  text: kOngingProjectsText,
                ),
                Tab(
                  text: kCompletedProjectsText,
                )
              ]),
        ),
        decoration: BoxDecoration(
            gradient: new LinearGradient(
          colors: [
            Colors.grey.shade300,
            Colors.white70,
          ],
          stops: [0.03, 0.40],
          begin: Alignment.topCenter,
          end: Alignment.center,
        )),
      ),
    );
  }

  Widget buildMakeObservationHeader() {
    return GestureDetector(
      child: Text(
        kMakeObservationsText,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 18.0,
          color: Colors.green.shade500,
          decoration: TextDecoration.underline,
        ),
      ),
    );
  }
}
