import 'package:edu_bio_med/ui_components/auth_header.dart';
import 'package:edu_bio_med/ui_components/back_appBar.dart';
import 'package:edu_bio_med/ui_components/custom_text_form_field.dart';
import 'package:edu_bio_med/ui_components/logo.dart';
import 'package:edu_bio_med/ui_components/secondary_button.dart';
import 'package:edu_bio_med/utils/constants/text_constants.dart';
import 'package:flutter/material.dart';

class ForgotPasswordScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ForgotPassword();
  }
}

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: customBackAppBar(),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Logo(),
            SizedBox(
              height: 35.0,
            ),
            AuthHeader(
              heading: kForgotYourPasswordText,
              description: kForgotYourPasswordDescriptionText,
              descriptionPadding: 50.0,
            ),
            SizedBox(
              height: 50.0,
            ),
            CustomTextFormField(
              labelText: kEmailText,
              isPasswordField: false,
              inputType: TextInputType.emailAddress,
            ),
            SizedBox(
              height: 35.0,
            ),
            SecondaryButton(
              buttonLabel: kSendText,
              onPress: () {},
              padding: EdgeInsets.symmetric(horizontal: 40.0, vertical: 18.0),
            ),
          ],
        ),
      ),
    );
  }
}
